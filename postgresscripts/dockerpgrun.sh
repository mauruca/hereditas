#!/bin/bash
# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

if [ -z "$1" ]
  then
    echo "Informar o nome do usuário da base."
    exit 1
fi

if [ -z "$2" ]
  then
    echo "Informar senha do usuário da base."
    exit 1
fi

docker run -d --name hereditasdb -e PGDATA=/var/lib/postgresql/data/pgdata -e POSTGRES_DB=postgres -e POSTGRES_PASSWORD=$2 -e POSTGRES_USER=$1 --volumes-from hpgdata -p 5432:5432 postgres:alpine
