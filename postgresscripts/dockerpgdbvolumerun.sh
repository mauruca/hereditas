#!/bin/bash
# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

docker create -v /var/lib/postgresql/data/pgdata --name hpgdata postgres:alpine /bin/true
