#!/bin/bash
# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

docker run --volumes-from hpgdata -v $(pwd)/backup:/backup ubuntu tar cvf /backup/backup.tar /var/lib/postgresql/data/pgdata