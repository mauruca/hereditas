#!/bin/bash
# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

docker stop hereditasdb
docker rm hereditasdb
docker rm hpgdata
