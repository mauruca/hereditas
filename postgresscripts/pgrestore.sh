#!/bin/bash
# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

docker run --volumes-from hpgdata -v $(pwd)/backup:/backup ubuntu bash -c "cd /var/lib/postgresql/data/pgdata && tar xvf /backup/backup.tar"
