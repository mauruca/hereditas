# [Hereditas](http://www.hereditas.net.br/)

The system is based on [Django](https://www.djangoproject.com/) and [Django REST Framework](http://http://www.django-rest-framework.org/).

## Bugs and Issues

Have a bug or an issue with this theme? [Open a new issue](https://gitlab.com/mauruca/hereditas/issues) here on GitHub.

## Creator
Hereditas was created by and is maintained by **Mauricio Pinheiro**, Managing Partner at **Ultima Ratio Regis Informatica LTDA**.

* https://gitlab.com/mauruca

## Copyright and License
Copyright (C) 2015-2019 Mauricio Costa Pinheiro. All rights reserved.Check LICENSE file for details.
